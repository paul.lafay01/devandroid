package fr.unilasalle.devandroid.tp.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.unilasalle.devandroid.tp.R
import fr.unilasalle.devandroid.tp.response.Product

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewViewHolder>() {

    var data: List<Product> = listOf()
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }

    private lateinit var onClickListener: OnClickListener

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_detail_main_screen, parent, false)
        return RecyclerViewViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewViewHolder, position: Int) {
        val item: Product = data[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { onClickListener.onClick(item) }
    }

    fun setOnClickListener(onClickListener: OnClickListener) { this.onClickListener = onClickListener }

    interface OnClickListener { fun onClick(model: Product) }

}