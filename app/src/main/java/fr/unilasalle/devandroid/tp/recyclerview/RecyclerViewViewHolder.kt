package fr.unilasalle.devandroid.tp.recyclerview

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.unilasalle.devandroid.tp.R
import fr.unilasalle.devandroid.tp.databinding.ItemDetailMainScreenBinding
import fr.unilasalle.devandroid.tp.response.Product

class RecyclerViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding: ItemDetailMainScreenBinding = ItemDetailMainScreenBinding.bind(itemView)

    fun bind(item: Product) {
        binding.title.text = item.title
        binding.price.text = "Price : ${item.price}"
        binding.category.text = item.category
        binding.ratingCount.text = "Rate : ${item.rating.rate}"
        binding.ratingRate.text = "NB rate : ${item.rating.count}"
        Picasso.get().load(item.image).resize(400, 400).centerInside().into(binding.imageView)
    }

}