package fr.unilasalle.devandroid.tp.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.unilasalle.devandroid.tp.R

class HistoryScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_screen)
    }
}