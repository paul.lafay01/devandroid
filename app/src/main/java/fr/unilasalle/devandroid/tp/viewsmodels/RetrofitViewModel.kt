package fr.unilasalle.devandroid.tp.viewsmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.unilasalle.devandroid.tp.response.Product
import fr.unilasalle.devandroid.tp.services.RetrofitService
import kotlinx.coroutines.launch
import java.lang.Exception

class RetrofitViewModel(
    private val retrofitService: RetrofitService
) : ViewModel() {

    private val _products = MutableLiveData<List<Product>>()
    val products: LiveData<List<Product>> = _products

    private val _product = MutableLiveData<Product>()
    val product: LiveData<Product> = _product

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    fun getProducts() {
        viewModelScope.launch {
            try {
                retrofitService.getProducts().let {
                    _products.value = it
                }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getProductByCategory(category: String) {
        viewModelScope.launch {
            try {
                retrofitService.getProductsByCategory(category).let {
                    _products.value = it
                }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

    fun getProduct(id: Long) {
        viewModelScope.launch {
            try {
                retrofitService.getProduct(id).let {
                    _product.value = it
                }
            } catch (e: Exception) {
                _error.value = e.message
            }
        }
    }

}