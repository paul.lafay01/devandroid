package fr.unilasalle.devandroid.tp.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.squareup.picasso.Picasso
import fr.unilasalle.devandroid.tp.R
import fr.unilasalle.devandroid.tp.databinding.ActivityItemScreenBinding
import fr.unilasalle.devandroid.tp.response.Product
import fr.unilasalle.devandroid.tp.response.Rate

class ItemScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivityItemScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // toolbar
        setSupportActionBar(binding.toolbar)

        // bottom navigation bar
        binding.bottomNavigationView.setOnItemSelectedListener {
            bottomNavBarChangeActivity(it.itemId)
            true
        }

        // passage d'informations par l'intent
        val productId: Long = intent.extras?.getLong("productId") ?: throw IllegalArgumentException("productId not found")
        val productTitle: String = intent.extras?.getString("productTitle") ?: throw IllegalArgumentException("productTitle not found")
        val productPrice: Double = intent.extras?.getDouble("productPrice") ?: throw IllegalArgumentException("productPrice not found")
        val productDescription: String = intent.extras?.getString("productDescription") ?: throw IllegalArgumentException("productDescription not found")
        val productCategory: String = intent.extras?.getString("productCategory") ?: throw IllegalArgumentException("productCategory not found")
        val productImage: String = intent.extras?.getString("productImage") ?: throw IllegalArgumentException("productImage not found")
        val productRate: Float = intent.extras?.getFloat("productRate") ?: throw IllegalArgumentException("productRate not found")
        val productRateCount: Int = intent.extras?.getInt("productRateCount") ?: throw IllegalArgumentException("productRateCount not found")

        // au lancement de l'activité
        val product = Product(
            productId,
            productTitle,
            productPrice,
            productDescription,
            productCategory,
            productImage,
            Rate(
                productRate,
                productRateCount
            )
        )

        binding.itemDetailTitle.text = product.title
        binding.itemDetailDescription.text = product.description
        binding.itemDetailCategory.text = product.category
        binding.itemDetailPrice.text = "Price : " + product.price.toString()
        binding.itemDetailRate.text = "Rate : " + product.rating.rate.toString()
        binding.itemDetailRateCount.text = "NB rate : " + product.rating.count.toString()
        Picasso.get().load(product.image).resize(600, 600).centerInside().into(binding.itemDetailImage)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_item_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {

        R.id.itemBack -> {
            this.finish()
            true
        }

        R.id.itemAddToCart -> {
            Toast.makeText(this, "Added to cart", Toast.LENGTH_SHORT).show()
            true
        }

        else -> { super.onOptionsItemSelected(item) }

    }

    private fun bottomNavBarChangeActivity(itemId: Int) {
        val intent: Intent
        when (itemId) {
            R.id.itemCart -> intent = Intent(applicationContext, CartScreenActivity::class.java)
            R.id.itemHistory -> intent = Intent(applicationContext, HistoryScreenActivity::class.java)
            R.id.itemShop -> intent = Intent(applicationContext, MainScreenActivity::class.java)
            else -> throw Exception("Problème sur la sélection des boutons : Bottom nav bar")
        }
        startActivity(intent)
    }

}