package fr.unilasalle.devandroid.tp.response

data class Product (
    val id: Long,
    val title: String,
    val price: Double,
    val description: String,
    val category: String,
    val image: String,
    val rating: Rate
)

data class Rate (
    val rate: Float,
    val count: Int
)