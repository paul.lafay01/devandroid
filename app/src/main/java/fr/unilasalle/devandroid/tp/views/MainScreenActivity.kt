package fr.unilasalle.devandroid.tp.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import fr.unilasalle.devandroid.tp.R
import fr.unilasalle.devandroid.tp.databinding.ActivityMainScreenBinding
import fr.unilasalle.devandroid.tp.recyclerview.RecyclerViewAdapter
import fr.unilasalle.devandroid.tp.response.Product
import fr.unilasalle.devandroid.tp.services.RetrofitApi
import fr.unilasalle.devandroid.tp.services.RetrofitService
import fr.unilasalle.devandroid.tp.viewsmodels.RetrofitViewModel
import fr.unilasalle.devandroid.tp.viewsmodels.RetrofitViewModelFactory


class MainScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainScreenBinding
    private lateinit var viewModel: RetrofitViewModel
    private lateinit var retrofitService: RetrofitService
    private lateinit var adapter: RecyclerViewAdapter

    private var cat1: String = "men's clothing"
    private var cat2: String = "women's clothing"
    private var cat3: String = "jewelery"
    private var cat4: String = "electronics"

    private var lastCatCalled: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // toolbar
        setSupportActionBar(binding.toolbar)

        // bottom navigation bar
        binding.bottomNavigationView.setOnItemSelectedListener {
            bottomNavBarChangeActivity(it.itemId)
            true
        }

        // retrofit
        retrofitService = RetrofitApi.getService()
        viewModel = ViewModelProvider(
            this,
            RetrofitViewModelFactory(retrofitService)
        )[RetrofitViewModel::class.java]

        // recycler view
        adapter = RecyclerViewAdapter()
        binding.recyclerview.adapter = adapter
        adapter.setOnClickListener(object: RecyclerViewAdapter.OnClickListener {
            override fun onClick(model: Product) {
                val intent = Intent(applicationContext, ItemScreenActivity::class.java)
                intent.putExtra("productId", model.id)
                intent.putExtra("productTitle", model.title)
                intent.putExtra("productPrice", model.price)
                intent.putExtra("productDescription", model.description)
                intent.putExtra("productCategory", model.category)
                intent.putExtra("productImage", model.image)
                intent.putExtra("productRate", model.rating.rate)
                intent.putExtra("productRateCount", model.rating.count)
                startActivity(intent)
            }
        })

        // au lancement de l'activité
        getProducts()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {

        R.id.itemRefresh -> {
            getProducts(lastCatCalled)
            true
        }
        R.id.catAll -> {
            getProducts()
            true
        }
        R.id.cat1 -> {
            getProducts(cat1)
            true
        }
        R.id.cat2 -> {
            getProducts(cat2)
            true
        }
        R.id.cat3 -> {
            getProducts(cat3)
            true
        }
        R.id.cat4 -> {
            getProducts(cat4)
            true
        }

        else -> { super.onOptionsItemSelected(item) }
    }

    private fun bottomNavBarChangeActivity(itemId: Int) {
        if(itemId != R.id.itemShop) {
            val intent: Intent
            if(itemId == R.id.itemCart) intent = Intent(applicationContext, CartScreenActivity::class.java)
            else intent = Intent(applicationContext, HistoryScreenActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getProducts(category: String = "") {
        lastCatCalled = category
        if(category == "") viewModel.getProducts()
        else viewModel.getProductByCategory(category)
        viewModel.products.observe(this) { adapter.data = it }
    }

}