package fr.unilasalle.devandroid.tp.services

import fr.unilasalle.devandroid.tp.response.Product
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitService {

    @GET("/products")
    suspend fun getProducts(): List<Product>

    @GET("/products/category/{category}")
    suspend fun getProductsByCategory(@Path("category") category: String): List<Product>

    @GET("/products/{id}")
    suspend fun getProduct(@Path("id") id: Long): Product

}